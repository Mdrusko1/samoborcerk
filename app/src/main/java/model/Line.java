package model;

import android.os.Parcel;
import android.os.Parcelable;

public class Line implements Parcelable {

    private String line;
    private String name;
    private String rasporedHtml;

    public Line(String line, String name, String rasporedHtml) {
        this.line = line;
        this.name = name;
        this.rasporedHtml = rasporedHtml;
    }

    public Line() {
    }

    protected Line(Parcel in) {
        line = in.readString();
        name = in.readString();
        rasporedHtml = in.readString();
    }

    public static final Creator<Line> CREATOR = new Creator<Line>() {
        @Override
        public Line createFromParcel(Parcel in) {
            return new Line(in);
        }

        @Override
        public Line[] newArray(int size) {
            return new Line[size];
        }
    };

    public String getRasporedHtml() {
        return rasporedHtml;
    }

    public void setRasporedHtml(String rasporedHtml) {
        this.rasporedHtml = rasporedHtml;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(line);
        dest.writeString(name);
        dest.writeString(rasporedHtml);
    }
}
