package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import b4a.samoborcek.DetailsActivity;
import b4a.samoborcek.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.Line;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder> implements Filterable {

    private Context context;
    protected List<Line> list, filterList;
    private CustomFilter filter;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_line_number)
        TextView txtLineNumber;
        @BindView(R.id.txt_line_name)
        TextView txtLineName;

        private MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setPostList(List<Line> lineList) {
        this.list = lineList;

    }

    public RecycleViewAdapter(Context context, List<Line> postList) {
        this.context = context;
        this.list = postList;
        this.filterList = postList;
    }


    public long getItemId(int position) {
        return position;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Line line = list.get(position);
        holder.txtLineNumber.setText(line.getLine());
        holder.txtLineName.setText(line.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("lineObject", line);
                context.startActivity(intent);
            }
        });

    }

    //RETURN FILTER OBJ
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter((ArrayList<Line>) filterList, this);
        }

        return filter;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
