package b4a.samoborcek;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mobapphome.mahandroidupdater.tools.MAHUpdaterController;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import adapter.RecycleViewAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.Line;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecycleViewAdapter adapter;
    private List<Line> lineList = new ArrayList<>();
    private String url = "http://www.samoborcek.hr/vozni-red/";
    private String vozniRedTitle, vozniRedSubTitle;
    private String VOZNI_RED = "LJETO";
    private String deviceName = Build.MODEL;
    private String deviceManufacturer = Build.MANUFACTURER;
    private ConsentForm form;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.floating_search_view)
    FloatingSearchView floatingSearchView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.lbl_title)
    TextView lblTitle;
    @BindView(R.id.lbl_subtitle)
    TextView lblSubtitle;
    @BindView(R.id.indicator_no_data)
    RelativeLayout layoutNoData;
    @BindView(R.id.adViewMainActivity)
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        // Here is GDPR Simple Code
        final ConsentInformation consentInformation = ConsentInformation.getInstance(this);
        String[] publisherIds = {"pub-7321329231330301"};
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                // User's consent status successfully updated.
                Log.d("STATUS", consentStatus.name());

            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                // User's consent status failed to update.
            }
        });


        if (MainApp.showAds) {
            MobileAds.initialize(getApplicationContext(), "ca-app-pub-7321329231330301~4283314424");
            AdRequest adRequest = new AdRequest.Builder()
                    .build();

            mAdView.loadAd(adRequest);
        }


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        increaseSwipeEdgeOfDrawer(drawer);
        navigationView.setNavigationItemSelectedListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new RecycleViewAdapter(this, lineList);


        adapter.setPostList(lineList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        swipeRefreshLayout.setColorScheme(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                recyclerView.setAdapter(null);
                new Content().execute();
            }
        });


        floatingSearchView.setOnLeftMenuClickListener(new FloatingSearchView.OnLeftMenuClickListener() {
            @Override
            public void onMenuOpened() {
                drawer.openDrawer(Gravity.START);
                floatingSearchView.closeMenu(true);
            }

            @Override
            public void onMenuClosed() {
            }
        });


        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                adapter.getFilter().filter(newQuery);
            }
        });


        new Content().execute();
    }


    @SuppressLint("StaticFieldLeak")
    private class Content extends AsyncTask<Line, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MainApp.showProgressDialog(MainActivity.this).show();
            lineList.clear();
        }

        @Override
        protected Void doInBackground(Line... lines) {

            Document doc = null;
            ArrayList<Line> tmpLine = new ArrayList<>();

            try {

                doc = Jsoup.connect(url).get();
                Elements links = doc.getElementsByClass("tb_accordeon_item_title");

                int x = 0;

                for (Element link : links) {
                    Elements h3 = link.select("h3");
                    String nazivLinije = h3.text();


                    if (Character.isDigit(nazivLinije.charAt(0))) {

                        Element rasporedHtml = doc.getElementsByClass("tb_accordeon_item_content").get(x);
                        String strHtml = rasporedHtml.html();
                        Line line = new Line(nazivLinije.substring(0, nazivLinije.indexOf("-")), nazivLinije.substring(nazivLinije.indexOf("-") + 1, nazivLinije.length()).trim(), strHtml);
                        tmpLine.add(line);


                    } else {

                        Element rasporedHtml = doc.getElementsByClass("tb_accordeon_item_content").get(x);
                        String strHtml = rasporedHtml.html();
                        Line line = new Line("LINIJA: ", nazivLinije.substring(0, nazivLinije.length()).trim(), strHtml);
                        tmpLine.add(line);
                    }

                    x++;

                }


                if (VOZNI_RED.equals("ZIMA")) {

                    // ZIMSKI VOZNI RED
                    for (int i = 0; i < 32; i++) {
                        lineList.add(tmpLine.get(i));

                    }

                    Elements ljetni = doc.getElementsByClass("col-1-3");
                    for (Element ljet : ljetni) {
                        Elements vrijedi = ljet.select("p");
                        if (vrijedi.text().contains("Vrijedi")) {
                            vozniRedTitle = vrijedi.get(0).text();
                            vozniRedSubTitle = vrijedi.get(1).text();
                            break;
                        }
                    }

                }


                if (VOZNI_RED.equals("LJETO")) {

                    // LJETNI VOZNI RED
                    for (int i = 32; i < 64; i++) {
                        lineList.add(tmpLine.get(i));
                    }

                    Elements ljetni = doc.getElementsByClass("col-1-3");
                    for (Element ljet : ljetni) {
                        Elements vrijedi = ljet.select("p");
                        if (vrijedi.text().contains("Vrijedi")) {
                            vozniRedTitle = vrijedi.get(0).text();
                            vozniRedSubTitle = vrijedi.get(1).text();
                        }
                    }

                }


            } catch (
                    final IOException e)

            {
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainApp.hideProgressDialog(MainActivity.this);
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(MainActivity.this, "Greška kod dohvaćanja rasporeda\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        showNoDataLayout();

                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            lblTitle.setText(vozniRedTitle);
            lblSubtitle.setText(vozniRedSubTitle);

            if (VOZNI_RED.equals("LJETO")) {
                lblTitle.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_sun, 0, 0);

            }

            if (VOZNI_RED.equals("ZIMA")) {
                lblTitle.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_winter, 0, 0);
            }


            adapter.setPostList(lineList);
            MainApp.hideProgressDialog(MainActivity.this);
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.setAdapter(adapter);
            showNoDataLayout();
            MAHUpdaterController.init(MainActivity.this, "http://mdrusko1.square7.ch/samoborcek/ver.php");
        }

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu);

        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_ljetni) {
            VOZNI_RED = "LJETO";
            new Content().execute();

        } else if (id == R.id.menu_zimski) {
            VOZNI_RED = "ZIMA";
            new Content().execute();


        } else if (id == R.id.menu_chat) {

            // Chat
            Intent intent = new Intent(MainActivity.this, ChatActivity.class);
            startActivity(intent);

        } else if (id == R.id.menu_gdpr) {
            showGDPRDialog();

        } else if (id == R.id.menu_like) {

            // Like
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            final LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setPadding(10, 10, 10, 10);
            linearLayout.setGravity(Gravity.CENTER);
            final RatingBar rating = new RatingBar(this);
            linearLayout.addView(rating);
            rating.setRating(5);
            alertDialogBuilder.setTitle(R.string.app_name)
                    .setMessage("Stisni 5 zvjezdica, ostavi komentar i razveseli programera! ;-)")
                    .setView(linearLayout)
                    .setPositiveButton(R.string.ocijeni, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            //Rate app
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }

                        }

                    }).show();


        } else if (id == R.id.menu_share) {

            // Share
            PackageManager manager = this.getPackageManager();
            try {
                PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " v." + info.versionName);
                intent.putExtra(Intent.EXTRA_TEXT, "Skini aplikaciju ovdje:\n https://play.google.com/store/apps/details?id=b4a.samoborcek");

                startActivity(Intent.createChooser(intent, "Send Email"));

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        } else if (id == R.id.menu_bug_report) {

            // Bug report
            PackageManager manager = this.getPackageManager();
            try {
                PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:shark.marko@gmail.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Bug report: Samoborček vozni red v." + info.versionName);
                intent.putExtra(Intent.EXTRA_TEXT, "");

                StringBuilder emailBody = new StringBuilder("Device Name:  ");
                emailBody.append(deviceName).append("\n");
                emailBody.append("Device Manufacturer:  ").append(deviceManufacturer).append("\n");
                emailBody.append("---------------------------------------------" + "\n\n");
                intent.putExtra(Intent.EXTRA_TEXT, emailBody.toString());

                startActivity(intent);

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        } else if (id == R.id.menu_about) {

            startActivity(new Intent(this, AboutActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public static void increaseSwipeEdgeOfDrawer(DrawerLayout mDlSearchDrawer) {
        try {

            Field mDragger = mDlSearchDrawer.getClass().getDeclaredField("mLeftDragger");//mRightDragger or mLeftDragger based on Drawer Gravity
            mDragger.setAccessible(true);
            ViewDragHelper draggerObj = (ViewDragHelper) mDragger.get(mDlSearchDrawer);

            Field mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
            mEdgeSize.setAccessible(true);
            int edge = mEdgeSize.getInt(draggerObj);

            mEdgeSize.setInt(draggerObj, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showNoDataLayout() {
        if (lineList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            layoutNoData.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            layoutNoData.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MAHUpdaterController.end();
    }

    /// GDPR
    public void showGDPRDialog() {
        URL privacyUrl = null;
        try {
            // TODO: Replace with your app's privacy policy URL.
            privacyUrl = new URL("http://mdrusko1.square7.ch/samoborcek/privacy_policy_samoborcek.html");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            // Handle error.
        }

        form = new ConsentForm.Builder(this, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        // Consent form loaded successfully.
                        form.show();
                    }

                    @Override
                    public void onConsentFormOpened() {
                        // Consent form was displayed.
                    }

                    @Override
                    public void onConsentFormClosed(
                            ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                        // Consent form was closed.
                    }

                    @Override
                    public void onConsentFormError(String errorDescription) {
                        // Consent form error.
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        form.load();
        // ## End Code

    }
}
