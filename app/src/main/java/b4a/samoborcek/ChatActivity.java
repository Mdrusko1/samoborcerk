package b4a.samoborcek;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import adapter.MessageAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import model.ChatMessage;


public class ChatActivity extends AppCompatActivity {

    private static final int SIGN_IN_REQUEST_CODE = 111;

    private FirebaseListAdapter<ChatMessage> adapter;
    private ListView listView;
    private String loggedInUserName = "";
    private View activityMain;
    @BindView(R.id.adViewChatActivity)
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity_main);
        ButterKnife.bind(this);

        try {
            getSupportActionBar().setTitle("Samoborček chat grupa");
            getSupportActionBar().setSubtitle("Samo za korisnike aplikacije");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (MainApp.showAds) {
            MobileAds.initialize(getApplicationContext(), "ca-app-pub-7321329231330301~4283314424");
            AdRequest adRequest = new AdRequest.Builder()
                    .build();

            mAdView.loadAd(adRequest);
        }


        //find views by Ids
        FloatingActionButton fab = findViewById(R.id.fab);
        FloatingActionButton fabEmoji = findViewById(R.id.fabEmoji);
        final EmojiconEditText input = findViewById(R.id.input);
        activityMain = findViewById(R.id.activity_main);
        listView = findViewById(R.id.list);


        EmojIconActions emojIcon = new EmojIconActions(this, activityMain, input, fabEmoji, "#495C66", "#DCE1E2", "#E6EBEF");
        emojIcon.ShowEmojIcon();

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .build(), SIGN_IN_REQUEST_CODE);
        } else {
            // User is already signed in, show list of messages
            MainApp.showProgressDialog(ChatActivity.this).show();
            showAllOldMessages();
        }


        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input.getText().toString().trim().equals("")) {
                    Toast.makeText(ChatActivity.this, "Poruka je prazna!", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .push()
                            .setValue(new ChatMessage(input.getText().toString(),
                                    FirebaseAuth.getInstance().getCurrentUser().getDisplayName(),
                                    FirebaseAuth.getInstance().getCurrentUser().getUid())
                            );
                    input.setText("");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        if (item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(ChatActivity.this, "Uspješno odjavljeni!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Uspješna prijava!", Toast.LENGTH_LONG).show();
                showAllOldMessages();
            } else {
                Toast.makeText(this, "Greška kod prijave, pokušajte ponovno kasnije.", Toast.LENGTH_LONG).show();

                // Close the app
                finish();
            }
        }
    }

    private void showAllOldMessages() {

        loggedInUserName = FirebaseAuth.getInstance().getCurrentUser().getUid();

        adapter = new MessageAdapter(this, ChatMessage.class, R.layout.item_in_message,
                FirebaseDatabase.getInstance().getReference());

        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                // the first time you get here, hide the progress bar
                MainApp.hideProgressDialog(ChatActivity.this);
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
            }
        });

        listView.setAdapter(adapter);


    }


    public String getLoggedInUserName() {
        return loggedInUserName;
    }


    @Override
    protected void onStart() {
        super.onStart();
        //adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //adapter.stopListening();
    }

}
