package b4a.samoborcek;

import android.app.Dialog;
import android.content.Context;
import android.os.StrictMode;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;


public class MainApp extends android.app.Application {

    private static Dialog progressDialog;
    private static android.app.Application instance;
    private static GoogleAnalytics analytics;
    private static Tracker tracker;
    public static boolean showAds = true;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        analytics = GoogleAnalytics.getInstance(this);

        // TODO: Replace the tracker-id with your app one from https://www.google.com/analytics/web/
        tracker = analytics.newTracker("UA-121809815-1");

        // Provide unhandled exceptions reports. Do that first after creating the tracker
        tracker.enableExceptionReporting(true);

        // Enable Remarketing, Demographics & Interests reports
        // https://developers.google.com/analytics/devguides/collection/android/display-features
        tracker.enableAdvertisingIdCollection(true);

        // Enable automatic activity tracking for your app
        tracker.enableAutoActivityTracking(true);

    }

    public static Dialog showProgressDialog(Context context) {
        progressDialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        progressDialog.setContentView(R.layout.layout_loading);
        return progressDialog;
    }

    public static void hideProgressDialog(Context context) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static GoogleAnalytics analytics() {
        return analytics;
    }

    public static Tracker tracker() {
        return tracker;
    }

}
